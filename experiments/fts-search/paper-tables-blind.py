#! /usr/bin/env python
# -*- coding: utf-8 -*-

import itertools
import logging
import numpy
import os

from collections import defaultdict

from downward.experiment import FastDownwardExperiment
from downward.reports import PlanningReport
from downward.reports.absolute import AbsoluteReport
from downward.reports.compare import ComparativeReport
from downward.reports.scatter import ScatterPlotReport

from lab.reports import Attribute, geometric_mean, arithmetic_mean

from lab import tools

from common_setup import IssueExperiment

exp = FastDownwardExperiment()

REVISIONS = [
    'fts-search-base-v2',
    'd70c6c01de9c',
    'b55624a83c62',
]

def remove_revision(run):
    algo = run['algorithm']
    for rev in REVISIONS:
        algo = algo.replace('{}-'.format(rev), '')
    run['algorithm'] = algo
    return run

exp.add_fetcher('data/2019-05-22-astar-baseline-eval',filter=[remove_revision],filter_algorithm=[
    'astar-blind',
    'astarunit-blind',
])
exp.add_fetcher('data/2019-05-22-astar-blind-bisim-eval',filter=[remove_revision],merge=True)
exp.add_fetcher('data/2019-05-29-astar-blind-weakbisim-unitcost-eval',filter=[remove_revision],merge=True)

ms_algorithm_time = Attribute('ms_algorithm_time', absolute=False, min_wins=True, functions=[geometric_mean])
ms_atomic_algorithm_time = Attribute('ms_atomic_algorithm_time', absolute=False, min_wins=True, functions=[geometric_mean])
ms_memory_delta = Attribute('ms_memory_delta', absolute=False, min_wins=True)
fts_transformation_time = Attribute('fts_transformation_time', absolute=False, min_wins=True, functions=[geometric_mean])
transformed_task_variables = Attribute('transformed_task_variables', absolute=False, min_wins=True, functions=[sum])
transformed_task_labels = Attribute('transformed_task_labels', absolute=False, min_wins=True, functions=[sum])
transformed_task_facts = Attribute('transformed_task_facts', absolute=False, min_wins=True, functions=[sum])
transformed_task_transitions = Attribute('transformed_task_transitions', absolute=False, min_wins=True, functions=[sum])
fts_search_task_construction_time = Attribute('fts_search_task_construction_time', absolute=False, min_wins=True, functions=[geometric_mean])
search_task_variables = Attribute('search_task_variables', absolute=False, min_wins=True, functions=[sum])
search_task_labels = Attribute('search_task_labels', absolute=False, min_wins=True, functions=[sum])
search_task_facts = Attribute('search_task_facts', absolute=False, min_wins=True, functions=[sum])
search_task_transitions = Attribute('search_task_transitions', absolute=False, min_wins=True, functions=[sum])
fts_plan_reconstruction_time = Attribute('fts_plan_reconstruction_time', absolute=False, min_wins=True, functions=[geometric_mean])
atomic_task_constructed = Attribute('atomic_task_constructed', absolute=True, min_wins=False)
solved_without_search = Attribute('solved_without_search', absolute=True, min_wins=True)
extra_attributes = [
ms_algorithm_time,
    ms_atomic_algorithm_time,
    ms_memory_delta,
    fts_transformation_time,
    transformed_task_variables,
    transformed_task_labels,
    transformed_task_facts,
    transformed_task_transitions,
    fts_search_task_construction_time,
    search_task_variables,
    search_task_labels,
    search_task_facts,
    search_task_transitions,
    fts_plan_reconstruction_time,
    atomic_task_constructed,
    solved_without_search,
]

attributes = list(IssueExperiment.DEFAULT_TABLE_ATTRIBUTES)
attributes.extend(extra_attributes)

## Configs

all_configs=[
'astar-blind',

'astar-blind-atomic',
'astar-blind-transform-atomic-labelreduction',
'astar-blind-transform-atomic-bisim-labelreduction',
'astar-blind-transform-full-bisim-labelreduction-dfp100-t900',
'astar-blind-transform-full-bisim-labelreduction-dfp1000-t900',
'astar-blind-transform-full-bisim-labelreduction-dfp10000-t900',
'astar-blind-transform-full-bisim-labelreduction-miasm100-t900',
'astar-blind-transform-full-bisim-labelreduction-miasm1000-t900',
'astar-blind-transform-full-bisim-labelreduction-miasm10000-t900',

'astarunit-blind',

'astar-blind-transformunitcost-atomic-weakbisim-labelreduction',
'astar-blind-transformunitcost-full-weakbisim-labelreduction-dfp100-t900',
'astar-blind-transformunitcost-full-weakbisim-labelreduction-dfp1000-t900',
'astar-blind-transformunitcost-full-weakbisim-labelreduction-dfp10000-t900',
'astar-blind-transformunitcost-full-weakbisim-labelreduction-miasm100-t900',
'astar-blind-transformunitcost-full-weakbisim-labelreduction-miasm1000-t900',
'astar-blind-transformunitcost-full-weakbisim-labelreduction-miasm10000-t900',
]

## HTML reports

exp.add_report(AbsoluteReport(attributes=attributes,filter_algorithm=all_configs))

exp.add_report(AbsoluteReport(attributes=[fts_transformation_time,fts_search_task_construction_time,fts_plan_reconstruction_time],filter_algorithm=[
    'astar-blind-transform-atomic-labelreduction',
    'astar-blind-transform-atomic-bisim-labelreduction',
    'astar-blind-transform-full-bisim-labelreduction-dfp1000-t900',
    'astar-blind-transform-full-bisim-labelreduction-miasm1000-t900',
]),name='paper-configs-construction-time-gm')

fts_transformation_time = Attribute('fts_transformation_time', absolute=False, min_wins=True, functions=[arithmetic_mean])
fts_search_task_construction_time = Attribute('fts_search_task_construction_time', absolute=False, min_wins=True, functions=[arithmetic_mean])
fts_plan_reconstruction_time = Attribute('fts_plan_reconstruction_time', absolute=False, min_wins=True, functions=[arithmetic_mean])

exp.add_report(AbsoluteReport(attributes=[fts_transformation_time,fts_search_task_construction_time,fts_plan_reconstruction_time],filter_algorithm=[
    'astar-blind-transform-atomic-labelreduction',
    'astar-blind-transform-atomic-bisim-labelreduction',
    'astar-blind-transform-full-bisim-labelreduction-dfp1000-t900',
    'astar-blind-transform-full-bisim-labelreduction-miasm1000-t900',
]),name='paper-configs-construction-time-am')

class OracleScatterPlotReport(ScatterPlotReport):
    """
    A bad copy of ScatterPlotReport that computes the min over the second and
    third given algorithm.
    """
    def __init__(self, take_first_as_third_algo=False, **kwargs):
        ScatterPlotReport.__init__(self, **kwargs)
        self.take_first_as_third_algo = take_first_as_third_algo

    def _fill_categories(self, runs):
        # We discard the *runs* parameter.
        # Map category names to value tuples
        categories = defaultdict(list)
        for (domain, problem), runs in self.problem_runs.items():
            if self.take_first_as_third_algo:
                if len(runs) != 2:
                    continue
                run1, run2 = runs
                assert (run1['algorithm'] == self.algorithms[0] and
                        run2['algorithm'] == self.algorithms[1])
                val1 = run1.get(self.attribute)
                val2 = run2.get(self.attribute)
                if val1 is None and val2 is None:
                    continue
                if val1 is None:
                    oracle_val = val2
                elif val2 is None:
                    oracle_val = val1
                else:
                    oracle_val = min(val1, val2)
                category = self.get_category(run1, run2)
                categories[category].append((val1, oracle_val))
            else:
                if len(runs) != 3:
                    continue
                run1, run2, run3 = runs
                assert (run1['algorithm'] == self.algorithms[0] and
                        run2['algorithm'] == self.algorithms[1] and
                        run3['algorithm'] == self.algorithms[2])
                val1 = run1.get(self.attribute)
                val2 = run2.get(self.attribute)
                val3 = run3.get(self.attribute)
                if val1 is None and val2 is None and val3 is None:
                    continue
                if val2 is None:
                    oracle_val = val3
                elif val3 is None:
                    oracle_val = val2
                else:
                    oracle_val = min(val2, val3)
                category = self.get_category(run1, run2)
                categories[category].append((val1, oracle_val))
        return categories

    def write(self):
        if (self.take_first_as_third_algo and not len(self.algorithms) == 2) or (not self.take_first_as_third_algo and not len(self.algorithms) == 3):
            logging.critical(
                'Oracle Scatter plots need exactly 2 algorithms if take_first_as_third_algo is true, otherwise 3: %s' % self.algorithms)
        self.xlabel = self.xlabel or self.algorithms[0]
        self.ylabel = 'oracle'

        suffix = '.' + self.output_format
        if not self.outfile.endswith(suffix):
            self.outfile += suffix
        tools.makedirs(os.path.dirname(self.outfile))
        self._write_plot(self.runs.values(), self.outfile)

### Latex reports

## expansion plots for bisim
exp.add_report(
    ScatterPlotReport(
        filter_algorithm=[
            'astar-blind',
            'astar-blind-transform-atomic-bisim-labelreduction',
        ],
        # get_category=lambda run1, run2: run1['domain'],
        attributes=['expansions_until_last_jump'],
        format='tex',
    ),
    outfile=os.path.join(exp.eval_dir, 'astar-blind-vs-astar-blind-transform-atomic-bisim-labelreduction'),
)

exp.add_report(
    ScatterPlotReport(
        filter_algorithm=[
            'astar-blind',
            'astar-blind-transform-full-bisim-labelreduction-dfp1000-t900',
        ],
        # get_category=lambda run1, run2: run1['domain'],
        attributes=['expansions_until_last_jump'],
        format='tex',
    ),
    outfile=os.path.join(exp.eval_dir, 'astar-blind-vs-astar-blind-transform-full-bisim-labelreduction-dfp1000-t900'),
)

## expansion plots for unitcost weakbisim
exp.add_report(
    ScatterPlotReport(
        filter_algorithm=[
            'astarunit-blind',
            'astar-blind-transformunitcost-atomic-weakbisim-labelreduction',
        ],
        # get_category=lambda run1, run2: run1['domain'],
        attributes=['expansions_until_last_jump'],
        format='tex',
    ),
    outfile=os.path.join(exp.eval_dir, 'astarunit-blind-vs-astar-blind-transformunitcost-atomic-weakbisim-labelreduction'),
)

exp.add_report(
    ScatterPlotReport(
        filter_algorithm=[
            'astarunit-blind',
            'astar-blind-transformunitcost-full-weakbisim-labelreduction-dfp1000-t900',
        ],
        # get_category=lambda run1, run2: run1['domain'],
        attributes=['expansions_until_last_jump'],
        format='tex',
    ),
    outfile=os.path.join(exp.eval_dir, 'astarunit-blind-vs-astar-blind-transformunitcost-full-weakbisim-labelreduction-dfp1000-t900'),
)

exp.run_steps()
