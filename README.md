FTS Planner is a planner based on the FTS representation, following the ideas presented on 

A. Torralba and S. Sievers, Merge-and-Shrink Task Reformulation for Classical Planning, Proceedings of the 28th International Joint Conference on Artificial Intelligence (IJCAI'19), https://people.cs.aau.dk/~alto/papers/ijcai19.pdf

FTS Planner is based on Fast Downward. For documentation and contact information see http://www.fast-downward.org/.

The following directories are not part of Fast Downward as covered by this
license:

* ./src/search/ext

For the rest, the following license applies:

```
Fast Downward is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

Fast Downward is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
```
